import java.util.Scanner;

public class Mercadorias{
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){
        int qtdEstoque = 0;
        System.out.println("Digite quantos produtos quer cadastrar: ");
        int qtdProdCadast = leia.nextInt(); 
        String[] nomeProd = new String[qtdProdCadast];
        int[][] produtos = new int[qtdProdCadast][4];
        for(int i = 0; i < qtdProdCadast; i++){
            System.out.println("Digite o nome da mercadoria: ");
            nomeProd[i] = leia.next();              
            System.out.println("Quantidade mínima dessa mercadoria: ");
            produtos[i][0] = leia.nextInt();
            System.out.println("Quantidade atual dessa mercadoria: ");
            produtos[i][1] = leia.nextInt();
            System.out.println("Quantidade máxima dessa mercadoria: ");
            produtos[i][2] = leia.nextInt();
            System.out.println("Valor da unidade da mercadoria: ");
            produtos[i][3] = leia.nextInt();
            if(produtos[i][1] <= produtos[i][2] && produtos[i][1] > 0){
                System.out.println("Produto cadastrado");
            }else if((produtos[i][1] > produtos[i][2]) || (produtos[i][1] < 0)){
                while((produtos[i][1] > produtos[i][2]) || (produtos[i][1] < 0)){
                    System.out.println("Quantidades informadas estão erradas. Digite as novamente corretamente: ");
                    System.out.println("------------------------------------------------------------------");
                    System.out.println("Quantidade atual dessa mercadoria: ");
                    produtos[i][1] = leia.nextInt();
                    System.out.println("Quantidade máxima dessa mercadoria: ");
                    produtos[i][2] = leia.nextInt();
                }
                System.out.println("Produto cadastrado");
            }
        }
        System.out.println("---------------------------");  
        for(int i = 0; i < nomeProd.length; i++){
            System.out.println(nomeProd[i] + ":");
            for(int j = 0; j < 1; j++){
                System.out.println("A quantidade de produtos em estoque é " + produtos[i][1]);
                System.out.println("O valor total do estoque desse produto é R$" + (produtos[i][1] * produtos[i][3]) + ",00 reais");
            }
        }
        System.out.println("-----------------------------");
        for(int i = 0; i < nomeProd.length; i++){
            if(produtos[i][1] < produtos[i][2]){
                System.out.println("O produto "+ nomeProd[i] + " precisa ser reposto.");
            }else{
                System.out.println("O produto " + nomeProd[i] + " não precisa ser reposto");
            }
        }
    }    
}
