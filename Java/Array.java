import java.util.Scanner;

public class Array{
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){
        float[] notas1 = new float[5];
        float[] notas2 = new float[5];

        for(int i = 0; i < notas1.length; i++){
            System.out.println("Digite a primeira e segunda nota do aluno " + (i+1) + " respectivamente: ");
            notas1[i] = leia.nextFloat();
            notas2[i] = leia.nextFloat();
            while((notas1[i] < 0) || (notas2[i] < 0) || (notas1[i] > 10) || (notas2[i] > 10)){
                System.out.println("Notas inválidas do aluno " + (i+1) + ", digite-as novamente.");
                notas1[i] = leia.nextFloat();
                notas2[i] = leia.nextFloat();
            }
            System.out.println("A média final desse aluno: " + ((notas1[i]+notas2[i]) / 2));
            System.out.println("------------------------------------");
        }

        // if ((notas1[i] < 0) || (notas2[i] < 0)){
        //         System.out.println("Notas inválidas, são menores que 0 (zero). Digite-as novamente.");
        //         break;
        //     }else if((notas1[i] > 10) || (notas2[i] > 10)){
        //         System.out.println("Notas inválidas, são maiores que 10 (dez). Digite-as novamente.");
        //         break;
        //     }else{
        //         System.out.println("A média final desse aluno: " + ((notas1[i]+notas2[i]) / 2));
        //         System.out.println("------------------------------------");
        //     }
    }
}