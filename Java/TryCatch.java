import java.util.Scanner;
public class TryCatch{
    public static Scanner leia = new Scanner(System.in);

    public static void teste(int i) throws Exception {
        if(i < 0){
            throw new Exception("valor inválido para argumento");
        }else{
            System.out.println(i);
        }
    }
    public static void main(String[] args){
        int idade = 0;
        try{
            System.out.println("Digite a idade para ser armazenada");
            idade = Integer.parseInt(leia.next());
            teste(idade);
        }catch(Exception e){
            System.out.println(e.getMessage());
            idade = Integer.parseInt(leia.next());
        }//finally{
        //     if(idade > 0){
        //         System.out.println("Idade inserida: " + idade);
        //     }else{
        //         System.out.println("Problemas na inserção da idade");
        //     }
        // }
    }
}